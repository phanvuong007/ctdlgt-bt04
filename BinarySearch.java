import java.util.Arrays;
public class BinarySearch {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int x = Integer.parseInt(args[1]);
        int a[];
        a = new int[n];
        for(int i = 0;i < n;i++) {
            a[i] = (int) (Math.random() * n);
        }
   
        Arrays.sort(a);
        if(Search(a, x) == -1) System.out.println("So khong co trong mang");
        else {
            System.out.println("So can tim o vi tri a[ " + Search(a,x) + "]");
        }
        
    }
    
    public static int Search(int a[], int x) {
        int start = 0;
        int end = a.length;
        int index;
        int mid = a.length / 2;
        int i;
        while(start <= end) {
            if(x == a[mid]) {
                return mid;
            }
            else if(x < a[mid]) {
                end = mid - 1;
                mid = end / 2;
            }
            else if( x > a[mid]) {
                start = mid - 1;
                mid = (start + end) / 2;                
            }
        }
        return -1;
        
    }
}