package ctglgt.bt04;

public class ThreeMax {

    public static void main(String[] args) {
        int a[], n;
        n = Integer.parseInt(args[0]);
        a = new int[n];
        int i;
        for (i = 0; i < n; i++) {
            a[i] = (int) (Math.random() * n);
        }

        selectionSort(a);

        int max1, max2, max3, index = 0;
        max1 = max2 = max3 = a[n - 1];
        for (i = n - 2; i >= 0; i--) {
            if (a[i] != max1) {
                max2 = a[i];
                index = i;
                break;
            }
        }
        for (i = index; i >= 0; i--) {
            if (a[i] != max2) {
                max3 = a[i];
                break;
            }

        }
        System.out.println(max1 + " " + max2 + " " + max3);

    }


    public static void selectionSort(int a[]) {
        int j, k;
        for (j = 0; j < a.length; j++) {
            for (k = j; k < a.length; k++) {
                if (a[j] > a[k]) {
                    int temp;
                    temp = a[j];
                    a[j] = a[k];
                    a[k] = temp;
                }
            }
        }
    }
}